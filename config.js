var config_local = {
    // Customer module configs
    "db": {
        "server": "postgres://lol:lol@127.0.0.1",
        "database": "lol"
    }
}

var config_devel = {
    // Customer module configs
    "db": {
        "server": "postgres://lol:lol@10.211.55.70",
        "database": "lol"
    }
}

var config_docker = {
    // Customer module configs
    "db": {
        "server": "postgres://lol:lol@postgres_db",
        "database": "lol"
    }
}

// Select correct config
var config = null;

switch (process.env.STAGE) {
    case "DOCKER":
        config = config_docker;
        break;

    case "LOCAL":
        config = config_local;
        break;

    case "DEVEL":
        config = config_devel;
        break;

    default:
        config = config_devel;
}

// Build connection string
config.db.connectionString = config.db.server + "/" + config.db.database
console.log(config.db.connectionString)
module.exports = config;